package prueba.prueba.service;

import org.springframework.stereotype.Service;
import prueba.prueba.dao.FlowerCache;
import prueba.prueba.model.FlowerDTO;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class FlowersServiceImpl implements FlowersService {

    private static int idCounter = 1;

    private final FlowerCache flowerCache;

    public FlowersServiceImpl(FlowerCache flowerCache) {
        this.flowerCache = flowerCache;
    }


    //Metodo para guardar el registro de las flores
    @Override
    public void save(FlowerDTO flowerDTO) {
        FlowerDTO flowerDTO1 = new FlowerDTO();
        flowerDTO1.setName(flowerDTO.getName().concat("-kometsales"));
        flowerDTO1.setPrice(flowerDTO.getPrice());
        flowerDTO1.setId(idCounter++);
        flowerCache.getInstance().add(flowerDTO1);
    }

    //Metodo para buscar todo el registro y devuelve una lista ordenada
    @Override
    public List<FlowerDTO> findAllListFlowers() {
        sortFlowers();
        return flowerCache.getInstance().getSavedFlowers();
    }

    //Servicio que llama al metodo que retorna la lista por el precio
    @Override
    public List<FlowerDTO> findListFlowersToPrice() {
        return filterToPrice();
    }

    //Metodo que retorna la lista de flores que coinsiden con el nombre
    @Override
    public List<FlowerDTO> findListFlowersToName(String name) {
        List<FlowerDTO> tempList = new ArrayList<>(flowerCache.getInstance().getSavedFlowers());
        tempList = tempList.stream()
                .filter(flower -> flower.getName().contains(name))
                .collect(Collectors.toList());
        if (tempList != null) {
            return tempList;
        } else {
            return new ArrayList<>();
        }
    }

    //Metodo que elimina el registro en la lista por ID
    public String deleteFlower(Integer id) {
        Iterator<FlowerDTO> iterator = flowerCache.getInstance().getSavedFlowers().iterator();
        while (iterator.hasNext()) {
            FlowerDTO flower = iterator.next();
            if (flower.getId() == id) {
                iterator.remove();
                return "Eliminado";
            }
        }
        return "No encontrado";
    }

    //Metodo que se encarga de filtrar por ID
    @Override
    public FlowerDTO findById(FlowerDTO flowerDTO) {
        List<FlowerDTO> tempList = new ArrayList<>(flowerCache.getInstance().getSavedFlowers());
        tempList = tempList.stream()
                .filter(flower -> flower.getId() == flowerDTO.getId())
                .collect(Collectors.toList());
        if (tempList.size() > 0){
            deleteFlower(flowerDTO.getId());
            return updateFlower(flowerDTO);
        } else {
            return null;
        }
    }

    //Metodo encargado de actualizar el resgrito dentro de la lista
    @Override
    public FlowerDTO updateFlower(FlowerDTO flowerDTO) {
        FlowerDTO flowerDTO1 = new FlowerDTO();
        flowerDTO1.setName(flowerDTO.getName().concat("-kometsales"));
        flowerDTO1.setPrice(flowerDTO.getPrice());
        flowerDTO1.setId(flowerDTO.getId());
        flowerCache.getInstance().add(flowerDTO1);
        return flowerDTO1;
    }

    //Funcion que ordena la lista en orden alfebetico y descendente
    private void sortFlowers() {
        Collections.sort(flowerCache.getInstance().getSavedFlowers(), Comparator.comparing(FlowerDTO::getName).reversed());
    }


    //Funcion que filtra el regisdtro en la lista por el precio mayor de 20
    private List<FlowerDTO> filterToPrice() {
        List<FlowerDTO> tempList = new ArrayList<>(flowerCache.getInstance().getSavedFlowers());
        tempList = tempList.stream()
                .filter(flower -> flower.getPrice() > 20)
                .collect(Collectors.toList());

        if(tempList != null) {
            return tempList;
        } else {
            return new ArrayList<>();
        }

    }
}