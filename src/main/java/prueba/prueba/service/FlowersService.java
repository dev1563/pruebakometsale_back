package prueba.prueba.service;

import prueba.prueba.model.FlowerDTO;

import java.util.List;

public interface FlowersService {

    void save(FlowerDTO flowerDTO);
    List<FlowerDTO> findAllListFlowers();
    List<FlowerDTO> findListFlowersToPrice();
    List<FlowerDTO> findListFlowersToName(String name);
    String deleteFlower(Integer id);
    FlowerDTO findById(FlowerDTO flowerDTO);
    FlowerDTO updateFlower(FlowerDTO flowerDTO);

}
