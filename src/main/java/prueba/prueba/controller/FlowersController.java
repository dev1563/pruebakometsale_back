package prueba.prueba.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import prueba.prueba.model.FlowerDTO;
import prueba.prueba.service.FlowersService;

import java.util.ArrayList;
import java.util.List;

@RestController
public class FlowersController {

    private final FlowersService flowersService;

    private static final Logger logger = LoggerFactory.getLogger(FlowersController.class);

    private FlowersController(@Autowired FlowersService flowersService) {
        this.flowersService = flowersService;
    }

    @PostMapping(value = "/saveFlower", consumes = {"application/json"})
    public FlowerDTO saveFlower(@RequestBody FlowerDTO flowerDTO) {
        try {
            flowersService.save(flowerDTO);
            return new ResponseEntity<>(flowerDTO, HttpStatus.OK).getBody();
        } catch (Exception e) {
            logger.error("Error al guardar la flor. Detalles: {}", e.getMessage(), e);
            return new ResponseEntity<>(flowerDTO, HttpStatus.INTERNAL_SERVER_ERROR).getBody();
        }
    }

    @GetMapping(value = "/listSaved")
    public List<FlowerDTO> findListToOrder() {
        try {
            return new ResponseEntity<>(flowersService.findAllListFlowers(), HttpStatus.OK).getBody();
        } catch (Exception e) {
            logger.error("Error al consultar las flores. Detalles: {}", e.getMessage(), e);
            return new ResponseEntity<>(new ArrayList<FlowerDTO>(), HttpStatus.INTERNAL_SERVER_ERROR).getBody();
        }
    }

    @GetMapping(value = "/listPrice")
    public List<FlowerDTO> findListToPrice() {
        try {
            return new ResponseEntity<>(flowersService.findListFlowersToPrice(), HttpStatus.OK).getBody();
        } catch (Exception e) {
            logger.error("Error al consultar las flores por precio. Detalles: {}", e.getMessage(), e);
            return new ResponseEntity<>(new ArrayList<FlowerDTO>(), HttpStatus.INTERNAL_SERVER_ERROR).getBody();
        }
    }

    @GetMapping(value = "/listName/{name}")
    public List<FlowerDTO> findListToName(@PathVariable("name") String name) {
        try {
            return new ResponseEntity<>(flowersService.findListFlowersToName(name), HttpStatus.OK).getBody();
        } catch (Exception e) {
            logger.error("Error al consultar las flores por nombre. Detalles: {}", e.getMessage(), e);
            return new ResponseEntity<>(new ArrayList<FlowerDTO>(), HttpStatus.INTERNAL_SERVER_ERROR).getBody();
        }
    }

    @PutMapping(value = "/updateFlower", consumes = {"application/json"})
    public FlowerDTO updateFlowers(@RequestBody FlowerDTO flowerDTO) {
        try {
            flowersService.findById(flowerDTO);
            return new ResponseEntity<>(flowerDTO, HttpStatus.OK).getBody();
        } catch (Exception e) {
            logger.error("Error al actualizar las flores. Detalles: {}", e.getMessage(), e);
            return new ResponseEntity<>(flowerDTO, HttpStatus.INTERNAL_SERVER_ERROR).getBody();
        }
    }

    @DeleteMapping(value = "/delete/{id}")
    public String delteFlower(@PathVariable("id") Integer id) {
        try {
            return new ResponseEntity<>(flowersService.deleteFlower(id), HttpStatus.OK).getBody();
        } catch (Exception e) {
            logger.error("Error al eliminar. Detalles: {}", e.getMessage(), e);
            return new ResponseEntity<>("Error", HttpStatus.INTERNAL_SERVER_ERROR).getBody();
        }
    }

}