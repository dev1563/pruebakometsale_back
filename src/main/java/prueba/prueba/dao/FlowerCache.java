package prueba.prueba.dao;

import org.springframework.stereotype.Service;
import prueba.prueba.model.FlowerDTO;

import java.util.ArrayList;
import java.util.List;

@Service
public class FlowerCache {

    private static FlowerCache instance;
    private List<FlowerDTO> savedFlowers = new ArrayList<>();

    private FlowerCache() {
    }

    public FlowerCache getInstance() {
        if (instance == null) {
            instance = new FlowerCache();
        }
        return instance;
    }

    public void add(FlowerDTO flower) {
        savedFlowers.add(flower);
    }

    public List<FlowerDTO> getSavedFlowers() {
        if (savedFlowers != null) {
            return savedFlowers;
        } else {
            return new ArrayList<>();
        }
    }


}
